# Send to room id.
roomid = "145577_mine@conf.hipchat.com"

cron = require('cron').CronJob #Task Scheduling
feedparser = require 'feedparser' #RSS Parser
request = require 'request' #HTTP Request

module.exports = (robot) ->
    console.log(robot)

    addRSS = (url, callback) ->
        ###
        Checking RSS list and add.
        ###
        urls = robot.brain.data["#{roomid}_RSS-reader"]
        urls = [] if !urls?
        # Replacing single-byte and double-byte spaces.
        url = url.replace(/\ /g, "").replace(/\　/g, "")

        for item in urls
            if item == url
                # Already list exists, and exit.
                response.send """
                Already exists, #{url}
                to #{roomid}
                """
                return

        # Add RSS list.
        urls.push url
        # Persisting the data.
        robot.brain.data["#{roomid}_RSS-reader"] = urls
        robot.brain.save()
        response.send """
        Added, #{url}
        to #{roomid}
        """
        callback url # Call of callback method.

    removeRSS = (url, callback) ->
        ###
        Remove from the list a URL.
        ###
        urls = robot.brain.data["#{roomid}_RSS-reader"]
        nurls = []
        # Replacing single-byte and double-byte spaces.
        url = url.replace(/\ /g, "").replace(/\　/g, "")

        for item in urls
            if item != url
                # Unmatch.
                nurls.push item

        # Saving data.
        robot.brain.data["#{roomid}_RSS-reader"] = nurls
        robot.brain.save()
        callback url

    infoRSS = (url, callback) ->
        ###
        Call of callback request and RSS.
        Return meta to the callback.
        ###
        request(url)
            .pipe(new feedparser [])
            .on("error", console.log.bind console)
            .on("meta", (meta) -> callback url, meta)

    listRSS = (callback) ->
        ###
        Get the meta, to call the callback.
        ###
        urls = robot.brain.data["#{roomid}_RSS-reader"]
        for url in urls
            # Request RSS infomation.
            infoRSS url, (url, meta) ->
                callback url, meta

    readRSS = (url, callback) ->
        entries = []
        request(url)
            .pipe(new feedparser [])
            .on("error", console.log.bind console)
            .on("data", entries.push.bind entries)
            .on("end", ->
                lastEntries = {}
                for entry in entries
                    lastEntries[entry.link] = true
                    if robot.brain.data[url]? and not robot.brain.data[url][entry.link]?
                        callback entry
                robot.brain.data[url] = lastEntries
                robot.brain.save()
            )

    # Initialize hubot response for send message.
    response = new robot.Response(robot, {room: roomid})
    robot.hear /RSS ADD (.*)/i, (msg) ->
        # Checking the addtion of RSS url.
        addRSS msg.match[1], (url) ->
            # Request RSS infomation.
            infoRSS url, (url, meta) ->
                response.send """
                ================================================================
                Title: #{meta.title}
                Description: #{meta.description}
                Link: #{meta.link}
                RSS: #{url}
                """

    robot.hear /RSS LIST/i, (msg) ->
        # Show a list of RSS.
        listRSS (url, meta) ->
            response.send """
            ================================================================
            Title: #{meta.title}
            Description #{meta.description}
            Link: #{meta.link}
            RSS: #{url}
            """

    robot.hear /RSS REMOVE (.*)/i, (msg) ->
        # Remove from the list a URL.
        removeRSS msg.match[1], (url) ->
            response.send """
            Removed, #{url}
            to #{roomid}
            """

    robot.hear /RSS HELP/i, (msg) ->
        # RSS reader help.
        response.send """
        This plug-in notification regularly RSS.
        [Usage]
          # Add URL to the RSS list.
          > rss add {RSS URL}
          # Remove URL from RSS list.
          > rss remove {RSS URL}
          # Show RSS list.
          > rss list
        [Repository]
          https://bitbucket.org/yukpiz/hubot-rss
        """

    # Initialize cron.
    new cron("*/1 * * * *", =>
        urls = robot.brain.data["#{roomid}_RSS-reader"]
        for url in urls
            readRSS url, (entry) ->
                response.send """
                RSS Reader =====================================================
                #{entry.title} #{entry.link}
                """
    ).start()

